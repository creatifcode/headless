import { Router, Response, Request } from "express";
import CustomersController from "../http/customers/controllers/CustomersController";
import CustomerAddressRouter = require("./CustomerAddressRouter");

class Customers {
    router: Router;
    controller: CustomersController;

    constructor() {
        this.controller = new CustomersController;
    }

    setRoutes() {
        /**
         * Get all customers route
         */
        this.router.get('/', (req: Request, res: Response) => {
            this.controller.getAllCustomers().then(customers => {
                res.status(200);
                return res.json({
                    status: res.statusCode,
                    data: customers
                })
            }).catch(error => {
                res.status(500);
                return res.json({
                    status: res.statusCode,
                    data: {
                        code: error.errno,
                        message: error.sqlMessage
                    }
                });
            });
        });

        /**
         * Find customer by params
         */
        this.router.get('/find', (req: Request, res: Response) => {
            res.send(this.controller.findCustomer(req.query));
        });

        this.router.post('/find', (req: Request, res: Response) => {
            this.controller.findByName(req.query.firstname, req.query.lastname).then(customers => {
                res.status(200);
                return res.json({
                    status: res.statusCode,
                    count: customers.length,
                    data: customers
                });
            }).catch(error => {
                res.status(500);
                return res.json({
                    status: res.statusCode,
                    data: {
                        code: error.errno,
                        message: error.sqlMessage
                    }
                });
            });
        });

        /**
         * Get single customer route
         */
        this.router.get('/:id', (req: Request, res: Response) => {
            res.send(this.controller.getCustomerById(req.params.id));
        });

        /**
         * Create customer and return result
         */
        this.router.post('/', (req: Request, res: Response) => {
            this.controller.createCustomer(req.body).then(customer => {
                res.status(200);
                return res.json({
                    status: res.statusCode,
                    data: customer
                });
            }).catch(error => {
                res.status(500);
                return res.json({
                    status: res.statusCode,
                    data: {
                        code: error.errno,
                        message: error.sqlMessage
                    }
                });
            });
        });
    }

    getRoutes() {
        this.router = Router();
        this.setRoutes();

        return this.router;
    }
}
export = new Customers().getRoutes();
