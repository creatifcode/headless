import { Router, Response, Request } from "express";
import { Customer } from "../entities/customer";

class CustomerAddress {
    router: Router;

    constructor() {
    }

    setRoutes() {
        /**
         * Get all address route
         */
        this.router.get('/', (req: Request, res: Response) => {
            //     this.controller.getAllCustomers().then(customers => {
            //         res.status(200);
            //         return res.json({
            //             status: res.statusCode,
            //             data: customers
            //         })
            //     }).catch(error => {
            //         res.status(500);
            //         return res.json({
            //             status: res.statusCode,
            //             data: {
            //                 code: error.errno,
            //                 message: error.sqlMessage
            //             }
            //         });
            //     });{


            // });
            Customer.find().then(customer => {
                console.log(customer);
            });

            res.send("TODO: Implement customers address methods for customer with id " + req.params.id);
        });

    }

    getRoutes() {
        this.router = Router();
        this.setRoutes();

        return this.router;
    }
}
export = new CustomerAddress().getRoutes();