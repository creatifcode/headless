import * as express from 'express';
import * as helmet from 'helmet';
import * as compression from 'compression';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import {createConnection} from "typeorm";
import CustomersRouter = require('./routes/CustomersRouter');

class Server {

    public app: express.Application;


    constructor() {
        this.app = express();
        this.setupDBConnection();
        this.config();
        this.routes();
    }

    public config() {
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(bodyParser.json());
        this.app.use(compression());
        this.app.use(helmet());
        this.app.use(cors());
    }

    public routes(): void {
        const router = express.Router();

        this.app.use('/', router);
        this.app.use('/customers', CustomersRouter);
    }

    setupDBConnection(): void {
        const connection = createConnection();
        connection.catch(error => {
            console.log('TypeORM DB error: ' + error);
        });
    }
}

export default new Server().app;