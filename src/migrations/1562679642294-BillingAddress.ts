import {MigrationInterface, QueryRunner, Table, Column, PrimaryGeneratedColumn, TreeLevelColumn, TableColumn, TableForeignKey} from "typeorm";

export class BillingAddress1562679642294 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.createTable(new Table({
            name: "address",
            columns: [
            {
                name: "id",
                type: "int",
                isPrimary: true,
                isGenerated: true,
                generationStrategy: "increment"
            },{
                name: "salutation",
                type: "varchar",
                length: "16",
                isNullable: true
            },{
                name: "title",
                type: "varchar",
                length: "32",
                isNullable: true
            },{
                name: "firstname",
                type: "varchar",
                length: "256"
            },{
                name: "lastname",
                type: "varchar",
                length: "256"
            },{
                name: "street",
                type: "varchar",
                length: "256"
            },{
                name: "zipcode",
                type: "varchar",
                length: "16"
            },{
                name: "city",
                type: "varchar",
                length: "64"
            },{
                name: "phone",
                type: "varchar",
                length: "32",
                isNullable: true
            },{
                name: "company",
                type: "varchar",
                length: "256",
                isNullable: true
            },{
                name: "vatId",
                type: "varchar",
                length: "32",
                isNullable: true
            },{
                name: "department",
                type: "varchar",
                length: "32",
                isNullable: true
            },{
                name: "additionalAddressLine1",
                type: "varchar",
                length: "256",
                isNullable: true
            },{
                name: "additionalAddressLine2",
                type: "varchar",
                length: "256",
                isNullable: true
            }
            ]
        }), true);

        queryRunner.addColumn('address', new TableColumn({
            name: "customerId",
            type: "int"
        }));

        queryRunner.createForeignKey('address', new TableForeignKey({
            columnNames: ['customerId'],
            referencedColumnNames: ['id'],
            referencedTableName: 'customers',
            onDelete: "CASCADE"
        }));

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        queryRunner.dropTable('address');
    }

}
