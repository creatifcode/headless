import { MigrationInterface, QueryRunner, Table } from "typeorm";
import { isNull } from "util";

export class Customers1562570993082 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'customers',
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment"
                }, {

                    name: "salutation",
                    type: "varchar",
                    length: "16",
                    isNullable: true
                }, {
                    name: "title",
                    type: "varchar",
                    length: "32",
                    isNullable: true
                }, {
                    name: "firstname",
                    type: "varchar",
                    length: "256"
                }, {
                    name: "lastname",
                    type: "varchar",
                    length: "256"
                }, {
                    name: "email",
                    type: "varchar",
                    length: "256"
                }, {

                    name: "birthday",
                    type: "date",
                    isNullable: true
                }, {
                    name: "active",
                    type: "boolean",
                    default: "1"
                }, {
                    name: "hashPassword",
                    type: "varchar",
                    isNullable: true,
                    length: "256"
                }, {
                    name: "firstLogin",
                    type: "date",
                    isNullable: true
                }, {
                    name: "lastLogin",
                    type: "datetime",
                    isNullable: true
                }, {
                    name: "confirmationKey",
                    type: "varchar",
                    length: "256",
                    isNullable: true
                }, {

                    name: "internalComment",
                    type: "text",
                    isNullable: true
                }, {
                    name: "failedLogins",
                    type: "int",
                    default: 0
                }, {

                    name: "lockedUntil",
                    type: "datetime",
                    isNullable: true
                }, {
                    name: "created_at",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                }, {
                    name: "updated_at",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                }
            ]
        }), true)
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        queryRunner.dropTable('customers');
    }

}
