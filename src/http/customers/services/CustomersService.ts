import CustomersRepository from "../repository/CustomersRepository";
import { Customer } from "../../../entities/Customer";

export default class CustomersService {
    repository: CustomersRepository;

    constructor() {
        this.repository = new CustomersRepository();
    }

    getAllCustomers() {
        return this.repository.getAllCustomers();
    }

    findByName(firstname: string, lastname?: string) {
        return this.repository.findByName(firstname, lastname);
    }

    createCustomer(customer: Customer) {
        return this.repository.createCustomer(customer);
    }
}