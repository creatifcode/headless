import { Customer } from "../../../entities/Customer";

export default class CustomersRepository {

    getAllCustomers() {
        return Customer
            .find();
    }

    findByName(firstname: string, lastname?: string) {
        return Customer.findByName(firstname, lastname);
    }

    createCustomer(customer: Customer) {
        return Customer
            .create(customer)
            .save();;
    }
}