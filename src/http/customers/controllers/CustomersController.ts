import CustomersService from "../services/CustomersService";
import { Customer } from "../../../entities/Customer";

export default class CustomersController {
    customersService: CustomersService;

    constructor() {
        this.customersService = new CustomersService();
    }

    getAllCustomers() {
        return this.customersService.getAllCustomers();
    }

    getCustomerById(id: number) {
        return "TODO: Implement service to get single customer with ID: " + id;
    }

    findCustomer(params: any) {
        return "TODO: Implement service to find customers by params: " + params.first_name + " " + params.last_name;
    }

    findByName(firstname: string, lastname?: string) {
        return this.customersService.findByName(firstname, lastname);
    }

    createCustomer(customer: Customer) {
        return this.customersService.createCustomer(customer);
    }
}