import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, OneToMany, JoinTable } from "typeorm";
import { BillingAddress } from "./BillingAddress";

@Entity({ "name": "customers" })
export class Customer extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { length: 16 })
    salutation: string;

    @Column('varchar', { length: 32 })
    title: string;

    @Column('varchar', { length: 256 })
    firstname: string;

    @Column('varchar', { length: 256 })
    lastname: string;

    @Column('varchar', { length: 256 })
    email: string;

    @Column('date')
    birthday: Date;

    @Column('tinyint', { default: 1 })
    active: boolean;

    @Column('varchar', { length: 256, select: false })
    hashPassword: string;

    @Column('datetime')
    firstLogin: Date;

    @Column('datetime')
    lastLogin: Date;

    @Column('varchar', { length: 128 })
    confirmationKey: string;

    @Column('text')
    internalComment: string;

    @Column('int', { default: 0 })
    failedLogins: number;

    @Column('datetime')
    lockedUntil: Date;

    @Column('timestamp', { default: () => "CURENT_TIMESTAMP" })
    created_at: Date;

    @Column('timestamp', { default: () => "CURENT_TIMESTAMP" })
    updated_at: Date;

    @OneToMany(type => BillingAddress, billingAddress => billingAddress.customer)
    billingAddress: BillingAddress[];

    static findByName(firstName: string, lastName?: string) {
        let query = this.createQueryBuilder('customer')
            .where('customer.firstname = :firstName', { firstName });

        if (lastName)
            query
                .andWhere('customer.lastname = :lastName', { lastName });

        return query.getMany();
    }
}