import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne } from "typeorm";
import { Customer } from "./Customer";

@Entity({ "name": "address" })
export class BillingAddress extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('varchar', { length: 16 })
    salutation: string;

    @Column('varchar', { length: 32 })
    title: string;

    @Column('varchar', { length: 256 })
    firstname: string;

    @Column('varchar', { length: 256 })
    lastname: string;

    @Column('varchar', { length: 256 })
    street: string;

    @Column('varchar', { length: 16})
    zipcode: string;

    @Column('varchar', { length: 64 })
    city: string;

    @Column('varchar', { length: 32 })
    phone: string;

    @Column('varchar', { length: 256 })
    company: string;

    @Column('varchar', { length: 32 })
    vatId: string;

    @Column('varchar', { length: 32 })
    department: string;

    @Column('varchar', { length: 256 })
    additionalAddressLine1: string;

    @Column('varchar', { length: 256 })
    additionalAddressLine2: string;

    @ManyToOne(type => Customer, customer => customer.billingAddress)
    customer: Customer;
}